from app.exceptions.user_exceptions import IdNotFound, InvalidPost, OtherError
from re import L
from pymongo import MongoClient
import datetime
from dotenv import load_dotenv
import os
from flask import request, json

from werkzeug.wrappers import request

# from_date =  datetime.strptime(from_date, '%Y-%m-%d')

client = MongoClient(os.getenv('DATABASE_URL'), int(os.getenv('DATABASE_PORT')))

db = client['kenzie']
collection =  db['posts']

class Post:

    def __init__(self, title: str, author: str, tags: list, content: str, updated_at = None):
        self.created_at: object =  datetime.datetime.now()
        self.updated_at: object = updated_at
        self.title: str = title
        self.author: str = author
        self.tags: list = tags
        self.content: str = content
        self.id = 1



    def create_id(self):
        posts = db.posts.count()
        self.id = posts + 1

    def save(self):
        _id = db.posts.insert_one(self.__dict__).inserted_id
        if not _id:
            raise InvalidPost
        new_post = db.posts.find_one({'_id': _id})
        del new_post['_id']
        return new_post

    @staticmethod
    def get_all():
        post_list = list(db.posts.find())
        for object in post_list:
            del object['_id']
        return post_list

    @staticmethod
    def find_posts(id: int):
        post_list = db.posts.find_one({'id': id})
        if not post_list:
            raise IdNotFound
        del post_list['_id']
        return post_list
    

    @staticmethod
    def update_posts(id: int, request):
        request['updated_at'] = datetime.datetime.now()
        lista = ['title', 'author', 'tags', 'content']
        for key in request:
            print(key)
            if key in lista:
                data = db.posts.find_one_and_update({"id": id}, {"$set": request}) 
            # else:
            #     raise OtherError
        if not data:
            raise IdNotFound
        del data['_id']
        
    

    @staticmethod
    def delete(id: int):
        post = db.posts.find_one_and_delete({"id": id})

        if not post:
            raise IdNotFound
        del post['_id']
        return post

