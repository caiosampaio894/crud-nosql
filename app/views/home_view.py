from datetime import datetime
from os import execlp
import re
from flask import Flask, json, jsonify, request
from app.model import Post
from app.exceptions.user_exceptions import IdNotFound, InvalidPost
def home_view(app: Flask):
    

    @app.route('/posts', methods=['POST'])
    def create_post():
        data = request.json

        try:
            post = Post(**data)
            post.create_id()
            result = post.save()
            return result, 201
        except (InvalidPost, TypeError):
            return {'msg': 'Dados inválidos para criação do usuário'}, 400

    @app.route('/posts', methods=['GET'])
    def read_posts():
        post_list = Post.get_all()
        return jsonify(post_list), 200

    @app.route('/posts/<int:id>', methods=['GET'])
    def read_post_by_id(id: int):
        try:
            post_id = Post.find_posts(id)
            return post_id, 200
        except IdNotFound:
            return {'msg': 'Id não encontrado'}, 404
        

    @app.route('/posts/<int:id>', methods=['DELETE'])
    def delete_post_by_id(id: int):
        try:
            post_id = Post.delete(id)
            return post_id, 200
        except IdNotFound:
            return {'msg': 'Post não encontrado'}, 404
    

    @app.route('/update-post/<int:id>', methods=['PATCH'])
    def update_posts(id: int):
        try:
            post_id = Post.update_posts(id, request.get_json())
            new_post = Post.find_posts(id)
            return new_post, 200
        except IdNotFound:
            return {'msg': 'Post não encontrado'}, 404